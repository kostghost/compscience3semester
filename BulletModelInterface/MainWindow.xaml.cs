﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


using BulletModel;

namespace BulletModelInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
        string filepath = @"preferences.txt";
        string resultFilePath;
        string throwProgramFileName = @"bullet1.exe";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = "Document";
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt) | *.txt";

            bool? result = dlg.ShowDialog();

            if (result == true)
            {
                textBox_filepath.Text = dlg.FileName;
                filepath = dlg.FileName;


                //Загрузка параметров
                BulletWork myBullet = new BulletWork(@dlg.FileName);

                if (myBullet.LoadPreferencesFromFile())
                {
                    textBox_velocity.Text = myBullet.velocity.ToString();
                    textBox_angle.Text = myBullet.angle.ToString();
                    textBox_deltaTime.Text = myBullet.deltaTime.ToString();
                    textBox_resistCooficent.Text = myBullet.resistCooficent.ToString();
                    textBox_gravity.Text = myBullet.gravity.ToString();
                }
                else
                {
                    MessageBox.Show("Your file is not contain parameters for bullet throw. \n Please enter them");
                }
            }
        }

        private void button_savePreferences_Click(object sender, RoutedEventArgs e)
        {
            BulletWork myBullet = new BulletWork(@filepath);

            string temp = textBox_velocity.Text;
            myBullet.velocity = Convert.ToDouble(textBox_velocity.Text);
            myBullet.angle = Convert.ToDouble(textBox_angle.Text);
            myBullet.deltaTime = Convert.ToDouble(textBox_deltaTime.Text);
            myBullet.resistCooficent = Convert.ToDouble(textBox_resistCooficent.Text);
            myBullet.gravity = Convert.ToDouble(textBox_gravity.Text);

            if (myBullet.SaveCurrentPreferences())
            {
                MessageBox.Show("File was saved in  \n" + @filepath);
            }
        }

        private void button_launch_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = @throwProgramFileName;
            process.StartInfo.Arguments = "\"" + @filepath + "\"";

            if (radioButton_euler.IsChecked == true)
                process.StartInfo.Arguments = "\"" + @filepath + "\"" + " euler";
            else
                process.StartInfo.Arguments = "\"" + @filepath + "\"";

            process.Start();

            label_resultSavedTo.Visibility = Visibility.Visible;
            textBox_resultPath.Visibility = Visibility.Visible;
            button_openWith.Visibility = Visibility.Visible;

            resultFilePath = Environment.CurrentDirectory + @"\result.txt";
            textBox_resultPath.Text = resultFilePath;

        }

        private void button_openWith_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = @resultFilePath;
            process.Start();
        }
    }
}
