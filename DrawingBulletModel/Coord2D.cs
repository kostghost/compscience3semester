﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingBulletModel
{
    public struct Coord2D
    {
        public double x;
        public double y;

        public Coord2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
