﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingBulletModel
{
    public class TrajectoryBuilder
    {
        public double velocity;
        public double deltaTime;
        public double gravity;
        public double angle;
        public double resistCooficent;

        public Coord2D maxTrackCoords = new Coord2D(0, 0); //Хранятся максимумы по X и Y

        public List<Coord2D> movementLaw = new List<Coord2D>(); // хранятся зависимости Y от X
        public List<Coord2D> VProj = new List<Coord2D>(); // хранятся проекции скоростей

        public TrajectoryBuilder()
            {
               
            }

        public TrajectoryBuilder(double angle, double velocity, double deltaTime, double gravity, double resistCooficent)
        {
            this.angle = angle;
            this.velocity = velocity;
            this.deltaTime = deltaTime;
            this.gravity = gravity;
            this.resistCooficent = resistCooficent;
        }

        

        public void CalculateAndWrite()
        {
            //Начальные параметры
            movementLaw.Add(new Coord2D(0, 0));
            VProj.Add(new Coord2D(velocity * Math.Cos(Math.PI * angle / 180), velocity * Math.Sin(Math.PI * angle / 180)));
            var currentWProj = new Coord2D(-resistCooficent * VProj.Last().x * VProj.Last().x,
                                            -gravity - resistCooficent * VProj.Last().y * VProj.Last().y);
            //**

            // double currentTime = 0;
            int i = 1;
            while (movementLaw.Last().y >= 0)
            {

                double modV = Math.Sqrt(VProj.Last().x * VProj.Last().x + VProj.Last().y * VProj.Last().y);
                currentWProj.x = -resistCooficent * modV * VProj.Last().x;
                currentWProj.y = -gravity - resistCooficent * modV * VProj.Last().y;

                movementLaw.Add(new Coord2D(movementLaw.Last().x + VProj.Last().x * deltaTime,
                                             movementLaw.Last().y + VProj.Last().y * deltaTime));

                VProj.Add(new Coord2D(VProj.Last().x + currentWProj.x * deltaTime,
                                        VProj.Last().y + currentWProj.y * deltaTime));

               

                 //currentWProj.x = -resistCooficent * VProj.Last().x;
                 //currentWProj.y = -gravity - resistCooficent *  VProj.Last().y;
                


                //Console.WriteLine(movementLaw.ElementAt(i).x.ToString() + " ; " + movementLaw.ElementAt(i).y.ToString());

                //Считаем максимумы координат
                if (movementLaw.Last().x > maxTrackCoords.x)
                    maxTrackCoords.x = movementLaw.Last().x;

                if (movementLaw.Last().y > maxTrackCoords.y)
                    maxTrackCoords.y = movementLaw.Last().y;

                i++;
            }
        }
        
    }
}
