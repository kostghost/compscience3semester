﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace DrawingBulletModel
{


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DispatcherTimer timer;

        List<Coord2D> bulletTrack = new List<Coord2D>(); //Координаты полета пули
        int currentStep = 0;
        Coord2D maxTrackCoords = new Coord2D(0, 0);
        Coord2D coordsToPixelsMultiplier = new Coord2D(1, 1);

        bool isMouseLeftButtonDown = false;
       

        public MainWindow()
        {
            InitializeComponent();
            
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(10);
            timer.Tick += new EventHandler(timer_Tick);

        }
        
        private void canvas_main_MouseMove(object sender, MouseEventArgs e)
        {
            //показывает координаты указателя в label'e
            if (bulletTrack.Count() > 0)
                label_positions.Content = string.Format("X = {0:0.000}ед Y = {1:0.000}ед", e.GetPosition(canvas_main).X / coordsToPixelsMultiplier.x, (canvas_main.ActualHeight - e.GetPosition(canvas_main).Y) / coordsToPixelsMultiplier.y);

            if (isMouseLeftButtonDown)
            {
                //Рисует линию прицеливания
                line_mouse_parameters.X1 = 0;
                line_mouse_parameters.X2 = e.GetPosition(canvas_main).X;
                line_mouse_parameters.Y1 = canvas_main.ActualHeight;
                line_mouse_parameters.Y2 = e.GetPosition(canvas_main).Y;

                //вычисляет угол
                double angle = Math.Atan((canvas_main.ActualHeight - e.GetPosition(canvas_main).Y) / e.GetPosition(canvas_main).X) * 180 / Math.PI;
                textBox_angle.Text = angle.ToString();

                //вычисляет начальную скорость
                double velocity = canvas_main.ActualHeight - e.GetPosition(canvas_main).Y + e.GetPosition(canvas_main).X;
                textBox_velocity.Text = velocity.ToString();
            }
           
            

            //polyLine_graphic.Points.Add(new Point(e.GetPosition(canvas_main).X, e.GetPosition(canvas_main).Y));
        }
        
        public bool ReadBulletTrackFromFile(string filepath = @"result.txt")
        {
            bulletTrack.Clear();
            using (StreamReader file = new StreamReader(filepath))
            {
                string line;
                try
                {
                    while ((line = file.ReadLine()) != null)
                    {
                        string[] items = line.Split(';');

                        bulletTrack.Add(new Coord2D(Double.Parse(items[0]), Double.Parse(items[1])));

                        //Считаем максимумы координат
                        if (bulletTrack.Last().x > maxTrackCoords.x)
                            maxTrackCoords.x = bulletTrack.Last().x;

                        if (bulletTrack.Last().y > maxTrackCoords.y)
                            maxTrackCoords.y = bulletTrack.Last().y;
                    }
                }
                catch (IOException e)
                {
                    return false;
                }
                catch (IndexOutOfRangeException e)
                {
                    return false;
                }

            }
            return true;
        }

        public void CalculateBulletTrack()
        {
            TrajectoryBuilder trBuilder = new TrajectoryBuilder(Convert.ToDouble(textBox_angle.Text), Convert.ToDouble(textBox_velocity.Text), 
                                                                Convert.ToDouble(textBox_deltatime.Text), Convert.ToDouble(textBox_gravity.Text), 
                                                                Convert.ToDouble(textBox_resist.Text));

            trBuilder.CalculateAndWrite();
            bulletTrack = trBuilder.movementLaw; //VProj - посмотреть график производной
            maxTrackCoords = trBuilder.maxTrackCoords;

            label_parameters.Content = string.Format("maxX: {0:0.000} maxY: {1:0.000}", maxTrackCoords.x, maxTrackCoords.y);
        }

        public void DrawGraphic()
        {
            polyLine_graphic.Points.Clear();
            if (checkBox_isYMultiplier.IsChecked.Value == true)
                coordsToPixelsMultiplier = new Coord2D(canvas_main.ActualWidth / maxTrackCoords.x,  canvas_main.ActualHeight / maxTrackCoords.y);
            else
                coordsToPixelsMultiplier = new Coord2D(canvas_main.ActualWidth / maxTrackCoords.x, canvas_main.ActualWidth / maxTrackCoords.x);

            timer.Start();
            currentStep = 0;
           
        }

        public void AddStep()
        {
            if (currentStep < bulletTrack.Count())
            {
                polyLine_graphic.Points.Add(new Point(bulletTrack.ElementAt(currentStep).x * coordsToPixelsMultiplier.x, canvas_main.ActualHeight - bulletTrack.ElementAt(currentStep).y * coordsToPixelsMultiplier.y));

                currentStep++;
            }
            else
            {
                currentStep = 0;
                timer.Stop();
            }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if (ReadBulletTrackFromFile())
            {
                label_parameters.Content = string.Format("maxX: {0:0.000} maxY: {1:0.000}", maxTrackCoords.x, maxTrackCoords.y);
            }
            else
                MessageBox.Show("Полет снаряда НЕ БЫЛ прочитан из файла");

        }

        void timer_Tick(object sender, EventArgs e)
        {
            AddStep();
        }

        private void canvas_main_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isMouseLeftButtonDown = true;
        }

        private void canvas_main_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isMouseLeftButtonDown = false;

            CalculateBulletTrack();
            if (bulletTrack.Count() > 0)
                DrawGraphic();
        }
    }
}
