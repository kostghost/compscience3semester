﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BulletModel
{
    //Работает со значениями типа double
    //Разделитель в файле :
    public class Preferences
    {
        private Dictionary<string, object> pref = new Dictionary<string, object>(); //здесь хранятся настройки
        private string filename; //Путь к файлу настроек

        public Preferences()
        {
            this.filename = @"preferences.txt";
        }

        public Preferences(string filename)
        {
            this.filename = filename;
        }

        public void SetFilePatch(string filename)
        {
            this.filename = filename;
        }

        public void Add(string key, object value)
        {
            if (!pref.ContainsKey(key))
                pref.Add(key, value);
            else
            {
                pref.Remove(key);
                pref.Add(key, value);
            }
        }

        //возвращает null при отсутствии объекта с названием
        public object Get(string key)
        {
            object temp;
            if (pref.TryGetValue(key, out temp))
                return temp;
            else return null;
         }

        public bool IsExist(string key)
        {
            return pref.ContainsKey(key);
        }

        //TODO проверка на ошибки
        public bool SavePreferences()
        {
            using (StreamWriter file = new StreamWriter(filename, false))
            {
                foreach (var pair in pref)
                    file.WriteLine(pair.Key + ": \t" + pair.Value);
                
            }
            return true;
        }

        //загружает настройки из файла, Возвращает false если файл не найден, 
        public bool LoadPreferences()
        {
            if (File.Exists(filename))
            {
                using (StreamReader file = new StreamReader(filename))
                {
                    string line;
                    try {
                        while ((line = file.ReadLine()) != null)
                        {
                            string[] items = line.Split(':');

                            this.Add(items[0].Trim(), Double.Parse(items[1]));
                        }
                    }
                    catch(IOException e)
                    {
                        return false;
                    }
                    catch(IndexOutOfRangeException e)
                    {
                        return false;
                    }

                }
                return true;
            }
            else
            {
                Console.WriteLine("File " + filename + " is not exist");
                return false;
            }
        }
        
    }

    public struct Coord2D
    {
        public double x;
        public double y;

        public Coord2D(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public class BulletWork
    {
        public BulletWork()
        {
            pref = new Preferences();
        }

        public BulletWork(string filepatch)
        {
            pref = new Preferences(filepatch);
        }


        protected Preferences pref;
        public double velocity;
        public double deltaTime;
        public double gravity;
        public double angle;
        public double resistCooficent;         

        protected List<Coord2D> movementLaw = new List<Coord2D>(); // хранятся зависимости Y от X

        //Возвращает false при отсутствии нужных параметров в файле
        public bool LoadPreferencesFromFile()
        {
            if (pref.LoadPreferences() && pref.IsExist("velocity") && pref.IsExist("deltaTime") && pref.IsExist("gravity") && pref.IsExist("angle") && pref.IsExist("resistCooficent"))
            {
                velocity = (double)pref.Get("velocity");
                deltaTime = (double)pref.Get("deltaTime");
                gravity = (double)pref.Get("gravity");
                angle = (double)pref.Get("angle");
                resistCooficent = (double)pref.Get("resistCooficent");
                return true;
            }
            else
                return false;
            
        }
        
        //Сохранение считанного в файл настроек
        public bool SaveCurrentPreferences()
        {
            pref.Add("velocity", velocity);
            pref.Add("deltaTime", deltaTime);
            pref.Add("gravity", gravity);
            pref.Add("angle", angle);
            pref.Add("resistCooficent", resistCooficent);

            return pref.SavePreferences();
        }
        
        //Считывание настроек из консоли
        public void LoadPreferencesFromConsole()
        {
            Console.WriteLine("Введите начальную скорость полета: ");
            velocity = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите угол полета в градусах");
            angle = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите dt: ");
            deltaTime = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите гравитацию");
            gravity = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("Введите коофицент сопротивления");
            resistCooficent = Convert.ToDouble(Console.ReadLine());

            
            SaveCurrentPreferences();
        }
               
        //сохранение закона движения в файл
        public bool SaveToFile(string path)
        {
            if (movementLaw.Count() > 0)
            {
                using (StreamWriter file = new StreamWriter(path))
                {
                    foreach (var i in movementLaw)
                    {
                        file.WriteLine(i.x + ";" +  i.y);
                    }
                }
                return true;
            }
            return false;
        
        }

    }

    public class BulletWorkFromLaw : BulletWork
    {
        public BulletWorkFromLaw()
        {
            pref = new Preferences();
        }

        public BulletWorkFromLaw(string filepatch)
        {
            pref = new Preferences(filepatch);
        }

        private static double getXt(double velocity, double angle, double time, double resistCooficent)
        {
            double CosA = Math.Cos(Math.PI * angle / 180);
            return velocity * time * CosA /*- (resistCooficent * velocity * velocity * CosA * CosA * time * time / 2) */;
        }

        private static double getYt(double velocity, double angle, double time, double gravity, double resistCooficent)
        {
            double SinA = Math.Sin(Math.PI * angle / 180);
            return velocity * time * SinA - (gravity * time * time) / 2 /* - (resistCooficent * velocity * velocity * SinA * SinA * time * time / 2) */;
        }

        //Расчет через уравнение траектории и вывод на экран
        public void CalculateAndWrite()
        {
            double currentTime = 0;
            int i = 0;
            while (true)
            {
                currentTime += deltaTime;
                if (getYt(velocity, angle, currentTime, gravity, resistCooficent) < 0)
                    break;
                movementLaw.Add(new Coord2D(getXt(velocity, angle, currentTime, resistCooficent), getYt(velocity, angle, currentTime, gravity, resistCooficent)));

                Console.WriteLine(movementLaw.ElementAt(i).x.ToString() + " ; " + movementLaw.ElementAt(i).y.ToString());

                i++;
            }
        }
    }

    public class BulletWorkFromEuler : BulletWork
    {
        private List<Coord2D> VProj = new List<Coord2D>(); // хранятся проекции скоростей
        
        public void CalculateAndWrite()
        {
            //Начальные параметры
            movementLaw.Add(new Coord2D(0,0 ));
            VProj.Add(new Coord2D(velocity * Math.Cos(angle), velocity * Math.Sin(angle)));
            var currentWProj = new Coord2D( -resistCooficent * VProj.Last().x * VProj.Last().x,
                                            -gravity - resistCooficent * VProj.Last().y * VProj.Last().y);
            //**

           // double currentTime = 0;
            int i = 1;
            while (movementLaw.Last().y >= 0)
            {

                movementLaw.Add(new Coord2D( movementLaw.Last().x + VProj.Last().x * deltaTime,
                                             movementLaw.Last().y + VProj.Last().y * deltaTime));
                VProj.Add(new Coord2D(  VProj.Last().x + currentWProj.x * deltaTime,
                                        VProj.Last().y + currentWProj.y * deltaTime));

                currentWProj.x = -resistCooficent * VProj.Last().x * VProj.Last().x;
                currentWProj.y = -gravity - resistCooficent * VProj.Last().y * VProj.Last().y;

                Console.WriteLine(movementLaw.ElementAt(i).x.ToString() + " ; " + movementLaw.ElementAt(i).y.ToString());

                i++;
            }
        }

        public BulletWorkFromEuler()
        {
            pref = new Preferences();
        }

        public BulletWorkFromEuler(string filepatch)
        {
            pref = new Preferences(filepatch);
        }



    }

    class Program
    {
        static void StartEuler(string patch = null)
        {
            BulletWorkFromEuler myBullet;

            if (patch != null)
                myBullet = new BulletWorkFromEuler(patch);
            else
                myBullet = new BulletWorkFromEuler();

            if (!myBullet.LoadPreferencesFromFile()) myBullet.LoadPreferencesFromConsole();
            myBullet.CalculateAndWrite();
            myBullet.SaveToFile(@"result.txt");
        }

        static void StartTrajectory(string patch = null)
        {
            BulletWorkFromLaw myBullet;

            if (patch != null)
                myBullet = new BulletWorkFromLaw(patch);
            else
                myBullet = new BulletWorkFromLaw();

            if (!myBullet.LoadPreferencesFromFile()) myBullet.LoadPreferencesFromConsole();
            myBullet.CalculateAndWrite();
            myBullet.SaveToFile(@"result.txt");
        }


        static void Main(string[] args)
        {
            if (args.Length >= 2)
                StartEuler(args[0]);
            else
                if (args.Length == 1)
                    StartTrajectory(args[0]);
            else
                StartTrajectory();
            
        }
    }
}
